# Description: GUI toolkit for the Tcl scripting language
# URL: http://tcl.sourceforge.net/
# Maintainer: mara, mara at fail dot pp dot ua
# Depends on: zlib tcl zip

name=tk
version=8.6.16
release=1
source=(https://downloads.sourceforge.net/tcl/$name$version-src.tar.gz
        font-sizes.diff)

build() {
  cd $name$version/unix

  patch -p1 -d $SRC/$name$version -i $SRC/font-sizes.diff

  ./configure \
    ZIP_PROG=zip \
    --prefix=/usr \
    --mandir=/usr/share/man \
    --enable-threads \
    --disable-rpath \
    --enable-64bit

  make
  make INSTALL_ROOT=$PKG install install-private-headers

  ln -sf wish${version%.*} $PKG/usr/bin/wish
  ln -sf libtk${version%.*}.so $PKG/usr/lib/libtk.so

  # install private headers (FS#14388, FS#47616)
  cd ..
  for dir in compat generic generic/ttk unix; do
    install -dm755 $PKG/usr/include/tk-private/$dir
    install -m644 -t $PKG/usr/include/tk-private/$dir $dir/*.h
  done

  # remove buildroot traces
  sed -e "s#${SRC}/${name}${version}/unix#/usr/lib#" \
      -e "s#${SRC}/${name}${version}#/usr/include#" \
      -i "${PKG}/usr/lib/tkConfig.sh"

  # remove unrequired execute permissions (FS#75042)
  chmod -x $PKG/usr/lib/libtkstub${version%.*}.a
}
