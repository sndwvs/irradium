# Description: A lightweight approach to removing Google web service dependency
# URL: https://github.com/ungoogled-software/ungoogled-chromium
# Maintainer: mara, mara at fail dot pp dot ua
# Depends on: gtk3 nss brotli alsa-lib xdg-utils cups libgcrypt ttf-liberation dbus pulseaudio pciutils libva libffi desktop-file-utils hicolor-icon-theme krb5 python3 gn ninja clang lld gperf nodejs git

name=ungoogled-chromium
version=133.0.6943.98
_uc_version=$version-1
release=2
source=(https://commondatastorage.googleapis.com/chromium-browser-official/${name#*-}-$version.tar.xz
        https://github.com/ungoogled-software/$name/archive/$_uc_version.tar.gz
        chromium.default chromium.sh
        patches/0001-widevine-support-for-arm.patch
        patches/0002-Run-blink-bindings-generation-single-threaded.patch
        patches/0003-Fix-eu-strip-build-for-newer-GCC.patch
        patches/use-oauth2-client-switches-as-default.patch
        patches/compiler-rt-adjust-paths.patch
        patches/increase-fortify-level.patch
)

build() {

    _system_clang=1

    cd ${name#*-}-$version

    # Configuration
    CRFLAGS="CHROMIUM_FLAGS"
    CRUSERFLAGS="CHROMIUM_USER_FLAGS"

    # Possible replacements are listed in build/linux/unbundle/replace_gn_files.py
    # Keys are the names in the above script; values are the dependencies in Arch
    declare -gA _system_libs=(
      #[brotli]=brotli
      #[dav1d]=dav1d
      #[ffmpeg]=ffmpeg
      [flac]=flac
      [fontconfig]=fontconfig
      [freetype]=freetype2
      #[harfbuzz-ng]=harfbuzz
      #[icu]=icu
      #[jsoncpp]=jsoncpp  # needs libstdc++
      #[libaom]=aom
      #[libavif]=libavif  # needs -DAVIF_ENABLE_EXPERIMENTAL_GAIN_MAP=ON
      [libjpeg]=libjpeg-turbo
      [libpng]=libpng
      #[libvpx]=libvpx
      [libwebp]=libwebp
      [libxml]=libxml2
      [libxslt]=libxslt
      [opus]=opus
      #[re2]=re2          # needs libstdc++
      #[snappy]=snappy    # needs libstdc++
      #[woff2]=woff2      # needs libstdc++
      [zlib]=minizip
    )
    _unwanted_bundled_libs=(
      $(printf "%s\n" ${!_system_libs[@]} | sed 's/^libjpeg$/&_turbo/')
    )
    depends+=(${_system_libs[@]})

    # Google API keys (see https://www.chromium.org/developers/how-tos/api-keys)
    _google_api_key=" "

    # Allow building against system libraries in official builds
    sed -i 's/OFFICIAL_BUILD/GOOGLE_CHROME_BUILD/' \
      tools/generate_shim_headers/generate_shim_headers.py

    # Arch Linux ARM fixes
    patch -Np1 -i $SRC/0001-widevine-support-for-arm.patch || exit 1
    patch -Np1 -i $SRC/0002-Run-blink-bindings-generation-single-threaded.patch || exit 1
    patch -Np1 -i $SRC/0003-Fix-eu-strip-build-for-newer-GCC.patch || exit 1

    if [[ $(uname -m) == "armv7h" ]]; then
        export ALARM_NINJA_JOBS="4"
        export MAKEFLAGS="-j4"
    fi

    CFLAGS="${CFLAGS/_FORTIFY_SOURCE=3/_FORTIFY_SOURCE=2}" && CXXFLAGS="$CFLAGS"

    # Allow build to set march and options on AArch64 (crc, crypto)
    [[ $(uname -m) == "aarch64" ]] && CFLAGS=`echo $CFLAGS | sed -e 's/-march=armv8-a//'` && CXXFLAGS="$CFLAGS"

    # https://crbug.com/893950
    sed -i -e 's/\<xmlMalloc\>/malloc/' -e 's/\<xmlFree\>/free/' \
           -e '1i #include <cstdlib>' \
      third_party/blink/renderer/core/xml/*.cc \
      third_party/blink/renderer/core/xml/parser/xml_document_parser.cc \
      third_party/libxml/chromium/*.cc

    # Use the --oauth2-client-id= and --oauth2-client-secret= switches for
    # setting GOOGLE_DEFAULT_CLIENT_ID and GOOGLE_DEFAULT_CLIENT_SECRET at
    # runtime -- this allows signing into Chromium without baked-in values
    patch -Np1 -i $SRC/use-oauth2-client-switches-as-default.patch || exit 1

    # Upstream fixes

    # Allow libclang_rt.builtins from compiler-rt >= 16 to be used
    patch -Np1 -i $SRC/compiler-rt-adjust-paths.patch || exit 1

    # Increase _FORTIFY_SOURCE level to match Arch's default flags
    patch -Np1 -i $SRC/increase-fortify-level.patch || exit 1

    # Fixes for building with libstdc++ instead of libc++

    if (( !_system_clang )); then
        # Use prebuilt rust as system rust cannot be used due to the error:
        #   error: the option `Z` is only accepted on the nightly compiler
        ./tools/rust/update_rust.py

        # To link to rust libraries we need to compile with prebuilt clang
        ./tools/clang/scripts/update.py
    fi

    # Ungoogled Chromium changes
    UNGOOGLED_REPO="$SRC/$name-$_uc_version"
    UTILS="${UNGOOGLED_REPO}/utils"
    echo 'Pruning binaries'
    python3 "$UTILS/prune_binaries.py" ./ "$UNGOOGLED_REPO/pruning.list"
    echo 'Applying patches'
    python3 "$UTILS/patches.py" apply ./ "$UNGOOGLED_REPO/patches"
    echo 'Applying domain substitution'
    python3 "$UTILS/domain_substitution.py" apply -r "$UNGOOGLED_REPO/domain_regex.list" \
        -f "$UNGOOGLED_REPO/domain_substitution.list" -c domainsubcache.tar.gz ./

    # Link to system tools required by the build
    mkdir -p third_party/node/linux/node-linux-x64/bin/
    ln -s /usr/bin/node third_party/node/linux/node-linux-x64/bin/
    #ln -s /usr/bin/java third_party/jdk/current/bin/
    ln -s $(whereis java | sed 's:\s:\n:g' | grep bin) third_party/jdk/current/bin/

    # Remove bundled libraries for which we will use the system copies; this
    # *should* do what the remove_bundled_libraries.py script does, with the
    # added benefit of not having to list all the remaining libraries
    for _lib in ${_unwanted_bundled_libs[@]}; do
        find "third_party/$_lib" -type f \
          \! -path "third_party/$_lib/chromium/*" \
          \! -path "third_party/$_lib/google/*" \
          \! -path "third_party/harfbuzz-ng/utils/hb_scoped.h" \
          \! -regex '.*\.\(gn\|gni\|isolate\)' \
          -delete
    done

    python3 build/linux/unbundle/replace_gn_files.py \
      --system-libraries "${!_system_libs[@]}"

    # Rebuild eu-strip
    #pushd buildtools/third_party/eu-strip
    #./build.sh
    #popd

    if (( _system_clang )); then
      export CC=clang
      export CXX=clang++
      export AR=llvm-ar
      export NM=llvm-nm
    else
      local _clang_path="$PWD/third_party/llvm-build/Release+Asserts/bin"
      export CC=$_clang_path/clang
      export CXX=$_clang_path/clang++
      export AR=$_clang_path/llvm-ar
      export NM=$_clang_path/llvm-nm
    fi

    _flags=(
      'custom_toolchain="//build/toolchain/linux/unbundle:default"'
      'host_toolchain="//build/toolchain/linux/unbundle:default"'
      'clang_use_default_sample_profile=false'
      'use_allocator="none"'
      'is_official_build=true' # implies is_cfi=true on x86_64
      'symbol_level=0' # sufficient for backtraces on x86(_64)
      'is_cfi=false'
      'treat_warnings_as_errors=false'
      'disable_fieldtrial_testing_config=true'
      'blink_enable_generated_code_formatting=false'
      'ffmpeg_branding="Chrome"'
      'proprietary_codecs=true'
      #'rtc_use_pipewire=true'
      'link_pulseaudio=true'
      'use_custom_libcxx=true' # https://github.com/llvm/llvm-project/issues/61705
      'use_gnome_keyring=false'
      'use_qt=false' # look into enabling this
      'use_gold=false'
      'use_sysroot=false'
      'use_system_libffi=true'
      'enable_hangout_services_extension=true'
      'enable_widevine=true'
      'enable_nacl=false'
      'enable_rust=true'
      "google_api_key=\"$_google_api_key\""
    )

    if [[ -n ${_system_libs[icu]+set} ]]; then
      _flags+=('icu_use_data_file=false')
    fi

    # Append ungoogled chromium flags to _flags array
    readarray -t -O ${#_flags[@]} _flags < "${UNGOOGLED_REPO}/flags.gn"

    if (( _system_clang )); then
        local _clang_version=$(
          clang --version | grep -m1 version | sed 's/.* \([0-9]\+\).*/\1/')

      _flags+=(
        'clang_base_path="/usr"'
        'clang_use_chrome_plugins=false'
        "clang_version=\"$_clang_version\""
        #'chrome_pgo_phase=0' # needs newer clang to read the bundled PGO profile
      )

      # Allow the use of nightly features with stable Rust compiler
      # https://github.com/ungoogled-software/ungoogled-chromium/pull/2696#issuecomment-1918173198
      export RUSTC_BOOTSTRAP=1

      _flags+=(
        'rust_sysroot_absolute="/usr"'
        'rust_bindgen_root="/usr"'
        "rustc_version=\"$(rustc --version)\""
      )
    fi

    if [[  $(uname -m) == "armv7h" ]]; then
      _flags+=('use_thin_lto=false')
    fi

    # Facilitate deterministic builds (taken from build/config/compiler/BUILD.gn)
    CFLAGS+='   -Wno-builtin-macro-redefined'
    CXXFLAGS+=' -Wno-builtin-macro-redefined'
    CPPFLAGS+=' -D__DATE__=  -D__TIME__=  -D__TIMESTAMP__='

    # Do not warn about unknown warning options
    CFLAGS+='   -Wno-unknown-warning-option'
    CXXFLAGS+=' -Wno-unknown-warning-option'

    # Let Chromium set its own symbol level
    CFLAGS=${CFLAGS/-g }
    CXXFLAGS=${CXXFLAGS/-g }

    # https://github.com/ungoogled-software/ungoogled-chromium-archlinux/issues/123
    CFLAGS=${CFLAGS/-fexceptions}
    CFLAGS=${CFLAGS/-fcf-protection}
    CXXFLAGS=${CXXFLAGS/-fexceptions}
    CXXFLAGS=${CXXFLAGS/-fcf-protection}

    # This appears to cause random segfaults when combined with ThinLTO
    # https://bugs.archlinux.org/task/73518
    CFLAGS=${CFLAGS/-fstack-clash-protection}
    CXXFLAGS=${CXXFLAGS/-fstack-clash-protection}

    # https://crbug.com/957519#c122
    CXXFLAGS=${CXXFLAGS/-Wp,-D_GLIBCXX_ASSERTIONS}

    gn gen out/Release --args="${_flags[*]}"
    ninja -C out/Release chrome chrome_sandbox chromedriver || exit 1

    install -D out/Release/chrome "$PKG/usr/lib/chromium/chromium"
    install -Dm4755 out/Release/chrome_sandbox "$PKG/usr/lib/chromium/chrome-sandbox"

    install -Dm644 chrome/installer/linux/common/desktop.template \
      "$PKG/usr/share/applications/chromium.desktop"
    install -Dm644 chrome/app/resources/manpage.1.in \
      "$PKG/usr/share/man/man1/chromium.1"
    sed -i \
      -e "s/@@MENUNAME@@/Chromium/g" \
      -e "s/@@PACKAGE@@/chromium/g" \
      -e "s/@@USR_BIN_SYMLINK_NAME@@/chromium/g" \
      "$PKG/usr/share/applications/chromium.desktop" \
      "$PKG/usr/share/man/man1/chromium.1"

    # Fill in common Chrome/Chromium AppData template with Chromium info
    (
        tmpl_file=chrome/installer/linux/common/appdata.xml.template
        info_file=chrome/installer/linux/common/chromium-browser.info
        . $info_file; PACKAGE=chromium
        export $(grep -o '^[A-Z_]*' $info_file)
        sed -E -e 's/@@([A-Z_]*)@@/\${\1}/g' -e '/<update_contact>/d' $tmpl_file | envsubst
    ) \
    | install -Dm644 /dev/stdin "$PKG/usr/share/metainfo/chromium.appdata.xml"

    toplevel_files=(
      chrome_100_percent.pak
      chrome_200_percent.pak
      chrome_crashpad_handler
      resources.pak
      v8_context_snapshot.bin

      # ANGLE
      libEGL.so
      libGLESv2.so

      # SwiftShader ICD
      libvk_swiftshader.so
      libvulkan.so.1
      vk_swiftshader_icd.json
    )

    if [[ -z ${_system_libs[icu]+set} ]]; then
      toplevel_files+=(icudtl.dat)
    fi

    cp "${toplevel_files[@]/#/out/Release/}" "$PKG/usr/lib/chromium/"
    install -Dm644 -t "$PKG/usr/lib/chromium/locales" out/Release/locales/*.pak

    for size in 24 48 64 128 256; do
      install -Dm644 "chrome/app/theme/chromium/product_logo_$size.png" \
        "$PKG/usr/share/icons/hicolor/${size}x${size}/apps/chromium.png"
    done

    for size in 16 32; do
      install -Dm644 "chrome/app/theme/default_100_percent/chromium/product_logo_$size.png" \
        "$PKG/usr/share/icons/hicolor/${size}x${size}/apps/chromium.png"
    done

    # Install a wrapper script:
    install -D -m0755 $SRC/${name}.sh $PKG/usr/bin/${name}
    sed \
        -e "s/@NAME@/${name}/g" \
        -e "s/@LIBDIRSUFFIX@//g" \
        -e "s/@CRFLAGS@/${CRFLAGS}/g" \
        -e "s/@CRUSERFLAGS@/${CRUSERFLAGS}/g" \
        -i $PKG/usr/bin/${name}
    # Move the 'default' file into place.
    # This allows users to override command-line options:
    mkdir -p $PKG/etc/${name}
    cat $SRC/${name}.default | sed \
      -e "s/@NAME@/${name}/g" \
      -e "s/@CRFLAGS@/${CRFLAGS}/g" \
      -e "s/@CRUSERFLAGS@/${CRUSERFLAGS}/g" \
      -e "s/@LIBDIRSUFFIX@//g" \
      > $PKG/etc/${name}/00-default.conf

}
