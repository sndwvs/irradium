# Description: WebRTC library for Telegram-desktop
# URL: https://github.com/desktop-app/tg_owt
# Maintainer: mara, mara at fail dot pp dot ua
# Depends on: cmake ninja pipewire pulseaudio libjpeg-turbo libxcomposite openh264

name=tg_owt
version=20240730
release=1
_commit=85694952217a8e3c0e7b3c70a8354c083ac5f010
_libsrtp_commit=a566a9cfcd619e8327784aa7cff4a1276dc1e895
_libyuv_commit=04821d1e7d60845525e8db55c7bcd41ef5be9406
_libvpx_commit=3688b17187d40a71f9a7de4bfac92201f3e4a533
_abseil_commit=4447c7562e3bc702ade25105912dce503f0c4010
_crc32c_commit=21fc8ef30415a635e7351ffa0e5d5367943d4a94
source=(https://github.com/desktop-app/tg_owt/archive/${_commit}.tar.gz
        https://chromium.googlesource.com/webm/libvpx/+archive/${_libvpx_commit}.tar.gz
        https://chromium.googlesource.com/libyuv/libyuv/+archive/${_libyuv_commit}.tar.gz
        https://github.com/google/crc32c/archive/${_crc32c_commit}.tar.gz
        https://github.com/abseil/abseil-cpp/archive/${_abseil_commit}.tar.gz
        https://github.com/cisco/libsrtp/archive/$_libsrtp_commit.tar.gz
        ffmpeg7.patch)

build() {
    cd $name-${_commit}

    mkdir -pv libyuv libvpx
    bsdtar xzf ../../../${_libyuv_commit}.tar.gz -C libyuv
    bsdtar xzf ../../../${_libvpx_commit}.tar.gz -C libvpx
    bsdtar xzf ../../../${_crc32c_commit}.tar.gz --strip-components 1 -C src/third_party/crc32c/src
    bsdtar xzf ../../../${_abseil_commit}.tar.gz --strip-components 1 -C src/third_party/abseil-cpp
    bsdtar xzf ../../../$_libsrtp_commit.tar.gz --strip-components 1 -C src/third_party/libsrtp/

    mv libyuv src/third_party/
    mv libvpx src/third_party/

    # path to openssl include is intentionally wrong, so that it will not mess up srtp include headers
    patch -Np1 -i $SRC/ffmpeg7.patch

    CXXFLAGS+=" -DLIBYUV_DISABLE_NEON"

    cmake -B build -G Ninja . \
        -DTG_OWT_PACKAGED_BUILD=True \
        -DBUILD_SHARED_LIBS=OFF \
        -DCMAKE_INSTALL_PREFIX=/usr \
        -DCMAKE_INSTALL_LIBDIR=lib \
        -DCMAKE_BUILD_TYPE=MinSizeRel
    cmake --build build
    DESTDIR=$PKG cmake --install build
}
