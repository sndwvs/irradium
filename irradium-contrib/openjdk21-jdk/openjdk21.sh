#!/bin/sh
export JAVA_HOME=/usr/lib/java/openjdk21-jdk
export MANPATH="${MANPATH}:${JAVA_HOME}/man"
export PATH="${PATH}:${JAVA_HOME}/bin"
