diff --git a/src/corelib/text/qchar.cpp b/src/corelib/text/qchar.cpp
index 7b114e9723..63296a92de 100644
--- a/src/corelib/text/qchar.cpp
+++ b/src/corelib/text/qchar.cpp
@@ -123,7 +123,9 @@ QT_BEGIN_NAMESPACE
 
     Starting with Qt 6.0, most QChar constructors are \c explicit. This
     is done to avoid dangerous mistakes when accidentally mixing
-    integral types and strings.
+    integral types and strings. You can opt-out (and make these
+    constructors implicit) by defining the macro \c
+    QT_IMPLICIT_QCHAR_CONSTRUCTION.
 
     For more information see
     \l{https://www.unicode.org/ucd/}{"About the Unicode Character Database"}.
@@ -2108,4 +2110,51 @@ static bool normalizationQuickCheckHelper(QString *str, QString::NormalizationFo
     return true;
 }
 
+/*!
+    \macro QT_IMPLICIT_QCHAR_CONSTRUCTION
+    \since 6.0
+    \relates QChar
+
+    Defining this macro makes certain QChar constructors implicit
+    rather than explicit. This is done to enforce safe conversions:
+
+    \badcode
+
+    QString str = getString();
+    if (str == 123) {
+        // Oops, meant str == "123". By default does not compile,
+        // *unless* this macro is defined, in which case, it's interpreted
+        // as `if (str == QChar(123))`, that is, `if (str == '{')`.
+        // Likely, not what we meant.
+    }
+
+    \endcode
+
+    This macro is provided to keep existing code working; it is
+    recommended to instead use explicit conversions and/or QLatin1Char.
+    For instance:
+
+    \code
+
+    QChar c1 =  'x'; // OK, unless QT_NO_CAST_FROM_ASCII is defined
+    QChar c2 = u'x'; // always OK, recommended
+    QChar c3 = QLatin1Char('x'); // always OK, recommended
+
+    // from int to 1 UTF-16 code unit: must guarantee that the input is <= 0xFFFF
+    QChar c4 = 120;        // compile error, unless QT_IMPLICIT_QCHAR_CONSTRUCTION is defined
+    QChar c5(120);         // OK (direct initialization)
+    auto  c6 = QChar(120); // ditto
+
+    // from int/char32_t to 1/2 UTF-16 code units:
+    // 𝄞 'MUSICAL SYMBOL G CLEF' (U+1D11E)
+    auto c7 = QChar(0x1D11E);           // compiles, but undefined behavior at runtime
+    auto c8 = QChar::fromUcs4(0x1D11E);       // always OK
+    auto c9 = QChar::fromUcs4(U'\U0001D11E'); // always OK
+    // => use c8/c9 as QStringView objects
+
+    \endcode
+
+    \sa QLatin1Char, QChar::fromUcs4, QT_NO_CAST_FROM_ASCII
+*/
+
 QT_END_NAMESPACE
diff --git a/src/corelib/text/qchar.h b/src/corelib/text/qchar.h
index b3fd794726..c0c53664c2 100644
--- a/src/corelib/text/qchar.h
+++ b/src/corelib/text/qchar.h
@@ -63,15 +63,17 @@ public:
     };
 
 #ifdef QT_IMPLICIT_QCHAR_CONSTRUCTION
-#error This macro has been removed in Qt 6.8.
+#define QCHAR_MAYBE_IMPLICIT Q_IMPLICIT
+#else
+#define QCHAR_MAYBE_IMPLICIT explicit
 #endif
 
     constexpr Q_IMPLICIT QChar() noexcept : ucs(0) {}
     constexpr Q_IMPLICIT QChar(ushort rc) noexcept : ucs(rc) {}
-    constexpr explicit QChar(uchar c, uchar r) noexcept : ucs(char16_t((r << 8) | c)) {}
+    constexpr QCHAR_MAYBE_IMPLICIT QChar(uchar c, uchar r) noexcept : ucs(char16_t((r << 8) | c)) {}
     constexpr Q_IMPLICIT QChar(short rc) noexcept : ucs(char16_t(rc)) {}
-    constexpr explicit QChar(uint rc) noexcept : ucs((Q_ASSERT(rc <= 0xffff), char16_t(rc))) {}
-    constexpr explicit QChar(int rc) noexcept : QChar(uint(rc)) {}
+    constexpr QCHAR_MAYBE_IMPLICIT QChar(uint rc) noexcept : ucs((Q_ASSERT(rc <= 0xffff), char16_t(rc))) {}
+    constexpr QCHAR_MAYBE_IMPLICIT QChar(int rc) noexcept : QChar(uint(rc)) {}
     constexpr Q_IMPLICIT QChar(SpecialCharacter s) noexcept : ucs(char16_t(s)) {}
     constexpr Q_IMPLICIT QChar(QLatin1Char ch) noexcept : ucs(ch.unicode()) {}
     constexpr Q_IMPLICIT QChar(char16_t ch) noexcept : ucs(ch) {}
@@ -83,10 +85,12 @@ public:
     // Always implicit -- allow for 'x' => QChar conversions
     QT_ASCII_CAST_WARN constexpr Q_IMPLICIT QChar(char c) noexcept : ucs(uchar(c)) { }
 #ifndef QT_RESTRICTED_CAST_FROM_ASCII
-    QT_ASCII_CAST_WARN constexpr explicit QChar(uchar c) noexcept : ucs(c) { }
+    QT_ASCII_CAST_WARN constexpr QCHAR_MAYBE_IMPLICIT QChar(uchar c) noexcept : ucs(c) { }
 #endif
 #endif
 
+#undef QCHAR_MAYBE_IMPLICIT
+
     static constexpr QChar fromUcs2(char16_t c) noexcept { return QChar{c}; }
     static constexpr inline auto fromUcs4(char32_t c) noexcept;
 
